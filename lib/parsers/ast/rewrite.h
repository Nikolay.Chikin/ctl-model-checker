#pragma once

#include <parsers/ast/common.h>
#include <parsers/ast/grammar.h>

#include <tao/pegtl/contrib/parse_tree.hpp>

node_ptr copy(const node_ptr& n);
node_ptr deep_copy(const node_ptr& n);
void fold(node_ptr& n);
void replace_or(node_ptr& n);
void replace_imply(node_ptr& n);
void replace_iff(node_ptr& n);

struct Rewrite : parse_tree::apply<Rewrite> {
    static void transform(node_ptr& n);
};

template<typename Rule>
struct Selector : parse_tree::selector<
    Rule,
    Rewrite::on<
        P0,
        P1,
        P2,
        P3,
        P4,
        P5
    >,
    parse_tree::store_content::on<
        Atom
    >,
    parse_tree::remove_content::on<
        True,
        False,
        Not,
        And,
        Or,
        Imply,
        IFF,
        EX,
        EG,
        EU
    >
> {};

template <>
struct Selector<AX> : std::true_type {
    static void transform(node_ptr& n);
};

template <>
struct Selector<AF> : std::true_type {
    static void transform(node_ptr& n);
};

template <>
struct Selector<EF> : std::true_type {
    static void transform(node_ptr& n);
};

template <>
struct Selector<AG> : std::true_type {
    static void transform(node_ptr& n);
};

template <>
struct Selector<AU> : std::true_type {
    static void transform(node_ptr& n);
};
