#pragma once

#include <parsers/ast/ast.h>

void rewrite_root(node_ptr& n);
void rewrite_top(node_ptr& n);
